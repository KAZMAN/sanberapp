import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';

import Component from './Latihan/Component/component'

export default function App() {
  return (
    <Component />
    // <ScrollView> 
    //  <View style={styles.container}>
    //   <Text> tulisan ini akan tampil!</Text>
    //   <StatusBar style="auto" />
    // </View> 
    // </ScrollView>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
