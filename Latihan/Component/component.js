import React, { Component } from 'react'
import { View,Text,Image, ScrollView, StyleSheet} from 'react-native'


export default class App extends Component {
  render() {
    return (

    <ScrollView style={styles.container}>
    
       <View style={styles.container}>
        <View style={styles.box} />
        <Text style={styles.text}>Hi..  Hello!</Text>
        <Image
        style={styles.image}
        source={{uri: 'http://www.reactnativeexpress.com/static/logo.png'}}/>
      </View>  
     </ScrollView>
     
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  box: {
    width: 200,
    height: 800,
    backgroundColor: 'darkblue',
    borderWidth: 2,
    borderColor: 'steelblue',
    borderRadius: 20,
  },

  text: {
    backgroundColor: 'whitesmoke',
    color: '#4A90E2',
    fontSize: 44,
    padding: 30,
  },

  image: {
    width: 200,
    height: 200,
  },
// })


// import React, { Component } from 'react'
// import { ScrollView, View, StyleSheet } from 'react-native'

// export default class App extends Component {
//   render() {
//     return (
//       <ScrollView style={styles.container}>
//         <View style={styles.boxLarge} />
//         <ScrollView horizontal>
//           <View style={styles.boxSmall} />
//           {/* <View style={styles.boxSmall} />
//           <View style={styles.boxSmall} /> */}
//         </ScrollView>
//         {/* <View style={styles.boxLarge} /> */}
//         <View style={styles.boxSmall} />
//         {/* <View style={styles.boxLarge} /> */}
//       </ScrollView>
//     )
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//   },
  boxSmall: {
    width: 200,
    height: 200,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'skyblue',
  },
  boxLarge: {
    width: 300,
    height: 300,
    marginBottom: 10,
    marginRight: 10,
    backgroundColor: 'steelblue',
  },
})